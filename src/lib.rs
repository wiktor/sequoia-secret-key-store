use parsec_client::core::interface::operations::psa_algorithm::{
    AsymmetricEncryption, AsymmetricSignature, Hash,
};
use parsec_client::BasicClient;

use openpgp::crypto::mpi::{Ciphertext, Signature};
use openpgp::crypto::{Decryptor, SessionKey, Signer};
use openpgp::packet::key::{PublicParts, UnspecifiedRole};
use openpgp::packet::Key;
use openpgp::types::HashAlgorithm;
use sequoia_openpgp as openpgp;

pub struct Client {
    parsec: BasicClient,
}

impl Client {
    pub fn new() -> Self {
        Client {
            parsec: BasicClient::new(Some("sequoia-secret-key-store".to_string()))
                .expect("Create to work"),
        }
    }

    pub fn ping(&self) {
        self.parsec.ping().expect("Ping to work");
    }

    pub fn get_key<'a, 'b>(&'a self, key_name: &'b str) -> ParsecKey<'a, 'b> {
        ParsecKey {
            parsec: &self.parsec,
            key_name,
        }
    }
}

pub struct ParsecKey<'a, 'b> {
    parsec: &'a BasicClient,
    key_name: &'b str,
}

impl<'a, 'b> ParsecKey<'a, 'b> {
    pub fn get_public_key(&self) -> Vec<u8> {
	self.parsec.psa_export_public_key(self.key_name).expect("Public key to be available")
    }
}

impl<'a, 'b> Signer for ParsecKey<'a, 'b> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        panic!("Not needed for RSA.");
    }

    fn sign(&mut self, hash_algo: HashAlgorithm, digest: &[u8]) -> openpgp::Result<Signature> {
        assert_eq!(hash_algo, HashAlgorithm::SHA256);
        Ok(Signature::RSA {
            s: self
                .parsec
                .psa_sign_hash(
                    self.key_name,
                    digest,
                    AsymmetricSignature::RsaPkcs1v15Sign {
                        hash_alg: Hash::Sha256.into(),
                    },
                )?
                .into(),
        })
    }
}

impl<'a, 'b> Decryptor for ParsecKey<'a, 'b> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        panic!("Not needed for RSA.");
    }

    fn decrypt(
        &mut self,
        ciphertext: &Ciphertext,
        _plaintext_len: Option<usize>,
    ) -> openpgp::Result<SessionKey> {
        if let Ciphertext::RSA { ref c } = ciphertext {
            Ok(self
                .parsec
                .psa_asymmetric_decrypt(
                    self.key_name,
                    AsymmetricEncryption::RsaPkcs1v15Crypt,
                    c.value(),
                    None,
                )?
                .into())
        } else {
            panic!("Not supported ciphertext type.");
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
